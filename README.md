# Python S3 object random access read support

The boto3 module provides file-like objects for reading S3 object contents. For example:
```
boto3.client('s3').get_object()['Body']
```
provides a `StreamingBody` object, which is a file-like object that only supports sequential read access.

Some S3 objects you may need to access are:
* Large; Hundreds of MB, GB, or a few TB in size
* Have their own internal format, like [FITS](http://docs.astropy.org/en/stable/io/fits/index.html) files.

In order to pull some data out of these kinds of files, you may need to first read one or more header sections in the file, then seek to one or more locations and read data. This non-sequential access isn't currently supported in the boto3 modules.

This module is meant to provide that random-access functionality for reading S3 objects, so that we don't need to download an entire S3 object.

## Example usage

1. Create an S3 client
```
import boto3
s3_client = boto3.client('s3')
```

2. Create a random-access file-like object for an S3 object we're interested in
```
from s3_randaccess import S3ObjectRandom
rand_fh = S3ObjectRandom(s3_client=s3_client, bucket='mybucket', key='123456.fits')
```

3. Pass the random-access file-like object to the library you're using to process it
```
# Newer versions of astropy can read only the needed sections of the file when working with it, instead
# of needing to access the entire file.
from astropy.io import fits
from astropy.nddata import Cutout2D
# Open fits file
hdulist = fits.open(rand_fh)
```

4. Use the astropy library to extract a small portion of the file
```
# Get cutout from 10th extension (assuming we knew how many there were before-hand. Could check with hdulist.info())
# http://astropy.readthedocs.io/en/latest/api/astropy.nddata.utils.Cutout2D.html#astropy.nddata.utils.Cutout2D
cutout = Cutout2D(hdulist[10].data, (1000, 1000), (1000, 1000))
# Write cutout to file
fits.writeto('banana_cutout_123456p.fits', cutout.data, hdulist[10].header)
```

In this example we were able to read from the file, downloading 3MB of data from a 330MB object instead of needing the entire object to perform the operation. 

## Usage notes

* Data read from the S3 object is buffered in-memory, so the amount of random-access reads you can do is limited by the memory you have available. 
* Which portions of the S3 object have been buffered is tracked with _chunks_; Byte-ranges of a certain size.
* If requested data is not within chunks that has been retrieved, it triggers the retrieveal of those chunks via a `Range` parameter in a `get_object` call. Subsequent reads for ranges covered by chunks uses the in-memory buffer instead of going to S3.
* Closing the file-handle releases the in-memory buffer.
