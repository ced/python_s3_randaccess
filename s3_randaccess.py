import io,math,tempfile

UNIT_MULTIPLIERS = {
    'byte':         1,
    'kilobyte':     1000,
    'kB':           1000,
    'kibibyte':     1024,
    'KiB':          1024,
    'KB':           1024,
    'megabyte':     1000**2,
    'MB':           1000**2,
    'mebibyte':     1024**2,
    'MiB':          1024**2,
    'gigabyte':     1000**3,
    'GB':           1000**3,
    'gibibyte':     1024**3,
    'GiB':          1024**3,
    'terabyte':     1000**4,
    'TB':           1000**4,
    'tebibyte':     1024**4,
    'TiB':          1024**4,
    'petabyte':     1000**5,
    'PB':           1000**5,
    'pebibyte':     1024**5,
    'PiB':          1024**5,
    'exabyte':      1000**6,
    'EB':           1000**6,
    'exbibyte':     1024**6,
    'EiB':          1024**6,
    'zettabyte':    1000**7,
    'ZB':           1000**7,
    'zebibyte':     1024**7,
    'ZiB':          1024**7,
    'yottabyte':    1000**8,
    'YB':           1000**8,
    'yobibyte':     1024**8,
    'YiB':          1024**8,
}

class S3ObjectRandom:
    """
    Provide read-only random access functionality for an S3 Object.

    s3_client should be an instance of boto3.client('s3')
    """

    def __init__(self, s3_client, bucket, key, chunk_size=1000**2, max_buffer_size=100 * UNIT_MULTIPLIERS['MB']):
        self.s3_client = s3_client
        self.bucket = bucket
        self.key = key
        self.chunk_size = chunk_size
        self.max_buffer_size = max_buffer_size
        # Buffer read bytes in memory, but if cache grows too large start using disk
        # NOTE(cedric): Rollover from memory to temporary file on disk behaviour is:
        # * Temporary file is created
        # * Contents written to temporary file
        # * Temporary file is deleted, but as long as handle is open it is still
        # accessible to this process.
        # * Once process ends or handle is closed, that temporary file is truly gone.
        #
        # This has the potential to cause visibility issues for disk utilization metrics
        # gathering, because program won't be able to tell where the disk usage for the
        # spooled file is coming from, unless it inspects the /proc filesytem for
        # its open handle to the deleted file.
        self.s3_object = tempfile.SpooledTemporaryFile(max_size=self.max_buffer_size, prefix='S3ObjectRandom_Spool-')

        self.s3_head = self.s3_client.head_object(Bucket=self.bucket, Key=self.key)
        self.s3_object_size = self.s3_head['ContentLength']
        self.chunks = self.chunk_ranges()

    def __del__(self):
        "Cleanup"
        self.close()

    def content_range_dict(self, text):
        """
        Decode an RFC2616 Content-Range field returned in an HTTP(S) response, and
        return a dictionary representing it.
        https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html

        Ex:
        content_range_dict('bytes 11000000-11610018/11610019')
        {'instance_length': 11610019, 'unit': 'bytes', 'last_byte_pos': 11610018, 'first_byte_pos': 11000000}

        In S3 API, instance_length seems to represent the total size of the object.
        """
        fields = {}
        (bytes_unit, _, range_spec) = text.partition(' ')
        fields['unit'] = bytes_unit
        if not '/' in range_spec:
            fields['instance_length'] = 'unknown'
            (first_byte_pos, _, last_byte_pos) = range_spec.partition('-')
            fields['first_byte_pos'] = int(first_byte_pos)
            fields['last_byte_pos'] = int(last_byte_pos)
        else:
            (byte_range_spec, _, instance_length) = range_spec.partition('/')
            (first_byte_pos, _, last_byte_pos) = byte_range_spec.partition('-')
            if instance_length == '*':
                fields['instance_length'] = 'unknown'
            else:
                fields['instance_length'] = int(instance_length)
            fields['first_byte_pos'] = int(first_byte_pos)
            fields['last_byte_pos'] = int(last_byte_pos)
        return fields

    def chunk_ranges(self):
        """
        Return a list of chunk byte ranges

        [(start, end, is_buffered, chunk_index), ...]
        """

        start = 0
        remaining = self.s3_object_size
        chunks = []
        while remaining > 0:
            if self.chunk_size > remaining:
                size = remaining
            else:
                size = self.chunk_size
            chunks.append((start, start + size - 1, False, len(chunks)))
            start += size
            remaining -= size
        return chunks

    def unbuffered_chunks(self, start=0, end=None):
        "Return a list of the chunks needed to be fetched, for the given byte range"
        if end is None:
            end = self.s3_object_size
        start_chunk = int(math.floor(start / self.chunk_size))
        end_chunk = int(math.ceil(end / self.chunk_size))
        return [c for c in self.chunks[start_chunk:end_chunk + 1] if not c[2]]

    def close(self):
        return self.s3_object.close()

    @property
    def closed(self):
        return self.s3_object.closed

    @property
    def _rolled(self):
        "Return True if buffer has rolled-over to a temporary file after exceeding max buffer size"
        if isinstance(self.s3_object, tempfile.SpooledTemporaryFile):
            return self.s3_object._rolled
        return False

    def _s3_read(self, start=0, end=None):
        "Return data from S3"
        # TODO(cedric): Split reads into 5GB chunks, if range of data is greater
        if end is None or end > self.s3_object_size:
            end = self.s3_object_size

        if start == 0 and end == self.s3_object_size:
            resp = self.s3_client.get_object(Bucket=self.bucket, Key=self.key)
            return (start, end, resp['Body'].read())

        byte_range = 'bytes={start}-{end}'.format(start=start, end=end)
        resp = self.s3_client.get_object(Bucket=self.bucket, Key=self.key, Range=byte_range)
        if 'ContentRange' in resp:
            content_range = self.content_range_dict(resp['ContentRange'])
            resp_start = content_range['first_byte_pos']
            resp_end = content_range['last_byte_pos']
        elif resp['ContentLength'] == self.s3_object_size:
            resp_start = 0
            resp_end = self.s3_object_size
        else:
            resp_start = start
            resp_end = start + resp['ContentLength']
        return (resp_start, resp_end, resp['Body'].read())

    def fetch_chunk(self, chunk):
        "Fetch and buffer the given chunk"
        (start, end, is_buffered, chunk_index) = chunk

        if is_buffered:
            return

        (data_start, data_end, data) = self._s3_read(start, end)
        self.seek(data_start)
        self.s3_object.write(data)

        # Mark affected chunks as buffered
        start_chunk = int(math.floor(data_start / self.chunk_size))
        end_chunk = int(math.floor(data_end / self.chunk_size)) # Partial chunk fetch is not a full chunk
        for (s, e, _, i) in self.chunks[start_chunk:end_chunk + 1]:
            self.chunks[i] = (s, e, True, i)

    def read(self, size=-1):
        "Read data through buffered S3 object"
        # If we're reading beyond the object size, return empty bytes
        if self.tell() >= self.s3_object_size:
            return b''

        # Negative or None size will read all data
        if size in (-1, None):
            # Fetch missing chunks from S3, then return entire buffer
            missing = self.unbuffered_chunks()
            while missing:
                self.fetch_chunk(missing[0])
                missing = self.unbuffered_chunks()
            self.seek(0)
            return self.s3_object.read(self.s3_object_size)

        # Fetch missing chunks from S3, then return the requested bytes
        pos = self.tell()
        end_pos = pos + size - 1
        missing = self.unbuffered_chunks(pos, end_pos)
        while missing:
            self.fetch_chunk(missing[0])
            missing = self.unbuffered_chunks(pos, end_pos)
        self.seek(pos)
        return self.s3_object.read(size)

    def readall(self):
        return self.read(size=-1)

    def seekable(self):
        return self.s3_object.seekable()

    def seek(self, offset, whence=io.SEEK_SET):
        if whence == io.SEEK_END:
            # The local buffer doesn't know what the size of the S3 object is,
            # so we provide it's end-of-stream value.
            return self.s3_object.seek(self.s3_object_size - abs(offset))
        return self.s3_object.seek(offset, whence)

    def peek(self, size=None):
        # Peek reads and reset the position, because for
        # io.BufferedRandom objects, peek() returns the current buffer.
        # You can get around this by slicing the returned buffer, but if
        # the current buffer is empty there's nothing for it to return, which is inaccurate.
        # Ex: self.s3_object.peek(size)[:size]
        if size is None:
            size = self.chunk_size

        pos = self.tell()
        for missing in self.unbuffered_chunks(pos, pos + size - 1):
            self.fetch_chunk(missing)
        self.seek(pos)
        data = self.s3_object.read(size)
        self.seek(pos)
        return data

    def tell(self):
        return self.s3_object.tell()
