import io, math, os, random, string, tempfile, unittest

import botocore
import boto3

from s3_randaccess import S3ObjectRandom

#S3_ENDPOINT = "https://your.wos.s3.host" # WOS S3
S3_ENDPOINT = "http://127.0.0.1:9000" # Minio S3
#S3_ENDPOINT = None # AWS S3

def random_string(length=13):
    "Return a random string of given length"
    random.seed()
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])

def s3_bucket_name(prefix='banana-'):
    """
    Return a valid S3 bucket name, that follows AWS conventions
    (http://docs.aws.amazon.com/AmazonS3/latest/dev/BucketRestrictions.html)
    """
    min_length = 3
    max_length = 63 - len(prefix)
    return prefix + random_string(random.randint(min_length, max_length)).lower()

def random_bytes(min_bytes=50000, max_bytes=150000):
    "Generate a byte stream with random contents, and return the file handle"
    file_size = random.randint(min_bytes, max_bytes)
    fh = io.BytesIO(os.urandom(file_size))
    fh.flush()
    return fh

def random_file(min_bytes=7 * 1000**2, max_bytes=15 * 1000**2):
    "Generate a file with random contents, and return the file name"
    file_size = random.randint(min_bytes, max_bytes)
    fh = tempfile.NamedTemporaryFile(prefix='test_s3_randaccess-', delete=False)
    fh.write(os.urandom(file_size))
    fh.close()
    return fh.name

class S3RandIO(unittest.TestCase):
    "Test S3ObjectRandom for equivalency of io.BytesIO read-related methods"
    @classmethod
    def setUpClass(self):
        self.s3_client = boto3.client('s3', endpoint_url=S3_ENDPOINT)
        self.s3 = boto3.resource('s3', endpoint_url=S3_ENDPOINT)
        self.s3_bucket_name = s3_bucket_name()
        #botocore.session.Session().get_config_variable('region')
        if S3_ENDPOINT is None:
            # NOTE(cedric): Some newer AWS S3 regions seem to require you to specify
            # the LocationConstraint when creating a bucket (ca-central-1), if not specified in the client connection.
            bucket_config = {'LocationConstraint': botocore.session.Session().get_config_variable('region')}
            self.s3_client.create_bucket(Bucket=self.s3_bucket_name,
                                         CreateBucketConfiguration=bucket_config)
        else:
            self.s3_client.create_bucket(Bucket=self.s3_bucket_name)

    @classmethod
    def tearDownClass(self):
        "Remove temporary objects, bucket created by tests"
        # WOS S3 doesn't support pagination for list_objects_v2 yet, so
        # use list_objects instead.
        paginator = self.s3_client.get_paginator('list_objects')
        for resp in paginator.paginate(Bucket=self.s3_bucket_name):
            if not 'Contents' in resp:
                continue
            for obj in resp['Contents']:
                self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=obj['Key'])
        self.s3_client.delete_bucket(Bucket=self.s3_bucket_name)

    def setUp(self):
        "Provide a random object, if needed by test"
        self.rand_key = random_string()
        fh = random_bytes()
        s3_obj = self.s3.Object(self.s3_bucket_name, self.rand_key)
        s3_obj.upload_fileobj(fh)
        self.rand_fh = S3ObjectRandom(s3_client=self.s3_client, bucket=self.s3_bucket_name, key=self.rand_key)

    def tearDown(self):
        self.rand_fh.close()
        self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=self.rand_key)

    def test_1_SeekTell(self):
        key = random_string()
        out_fh = io.BytesIO(bytes(range(20)))
        s3_obj = self.s3.Object(self.s3_bucket_name, key)
        s3_obj.upload_fileobj(out_fh)

        in_fh = S3ObjectRandom(s3_client=self.s3_client, bucket=self.s3_bucket_name, key=key)

        in_fh.seek(20)
        self.assertEqual(in_fh.tell(), 20)
        in_fh.seek(0)
        self.assertEqual(in_fh.tell(), 0)
        in_fh.seek(10)
        self.assertEqual(in_fh.tell(), 10)
        in_fh.seek(5, 1)
        self.assertEqual(in_fh.tell(), 15)
        in_fh.seek(-5, 1)
        self.assertEqual(in_fh.tell(), 10)
        in_fh.seek(-5, 2)
        self.assertEqual(in_fh.tell(), 15)

        in_fh.close()
        self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=key)

    def test_2_Attributes(self):
        self.assertEqual(self.rand_fh.closed, False)

    def test_3_none_args(self):
        key = random_string()
        out_fh = io.BytesIO(b"hi\nbye\nabc")
        s3_obj = self.s3.Object(self.s3_bucket_name, key)
        s3_obj.upload_fileobj(out_fh)

        in_fh = S3ObjectRandom(s3_client=self.s3_client, bucket=self.s3_bucket_name, key=key)

        self.assertEqual(in_fh.read(None), b"hi\nbye\nabc")
        in_fh.seek(0)
        # NOTE(cedric): readline, readlines not implemented yet!
        #self.assertEqual(in_fh.readline(None), b"hi\n")
        #self.assertEqual(in_fh.readlines(None), [b"bye\n", b"abc"])

        in_fh.close()
        self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=key)

class S3RandBuffer(unittest.TestCase):
    "Test S3ObjectRandom buffering behaviour, to ensure it behaves like a file object"
    @classmethod
    def setUpClass(self):
        self.s3_client = boto3.client('s3', endpoint_url=S3_ENDPOINT)
        self.s3 = boto3.resource('s3', endpoint_url=S3_ENDPOINT)
        self.s3_bucket_name = s3_bucket_name()
        if S3_ENDPOINT is None:
            bucket_config = {'LocationConstraint': botocore.session.Session().get_config_variable('region')}
            self.s3_client.create_bucket(Bucket=self.s3_bucket_name,
                                         CreateBucketConfiguration=bucket_config)
        else:
            self.s3_client.create_bucket(Bucket=self.s3_bucket_name)

    @classmethod
    def tearDownClass(self):
        "Remove temporary objects, bucket created by tests"
        # WOS S3 doesn't support pagination for list_objects_v2 yet, so
        # use list_objects instead.
        paginator = self.s3_client.get_paginator('list_objects')
        for resp in paginator.paginate(Bucket=self.s3_bucket_name):
            if not 'Contents' in resp:
                continue
            for obj in resp['Contents']:
                self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=obj['Key'])
        self.s3_client.delete_bucket(Bucket=self.s3_bucket_name)

    def setUp(self):
        "Provide a random object for test"
        self.local_file = random_file()
        self.local_fh = open(self.local_file, 'rb')
        self.file_size = os.stat(self.local_file).st_size

        self.key = random_string()
        s3_obj = self.s3.Object(self.s3_bucket_name, self.key)
        s3_obj.upload_file(self.local_file)
        self.s3_fh = S3ObjectRandom(s3_client=self.s3_client, bucket=self.s3_bucket_name, key=self.key)

    def tearDown(self):
        self.local_fh.close()
        self.s3_fh.close()
        os.unlink(self.local_file)
        self.s3_client.delete_object(Bucket=self.s3_bucket_name, Key=self.key)

    def test_1_BufferBehaviour(self):
        # Compare last byte
        self.local_fh.seek(self.file_size - 1)
        self.s3_fh.seek(self.file_size - 1)
        self.assertEqual(self.local_fh.read(1), self.s3_fh.read(1), 'last byte mismatch!')

        # Compare first byte
        self.local_fh.seek(0)
        self.s3_fh.seek(0)
        self.assertEqual(self.local_fh.read(1), self.s3_fh.read(1), 'first byte mismatch!')

        # Compare bytes at 3/4 way through file
        pos_75pct = int(math.floor(self.file_size * 0.75))
        len_10pct = int(math.floor(self.file_size * 0.1))
        self.local_fh.seek(pos_75pct)
        self.s3_fh.seek(pos_75pct)
        self.assertEqual(self.local_fh.read(len_10pct), self.s3_fh.read(len_10pct), '75-85pct byte range mismatch!')

        # Compare overlapping S3ObjectRandom chunk range
        to_previous_chunk = 0 - (self.s3_fh.chunk_size + 8)
        self.local_fh.seek(to_previous_chunk, io.SEEK_CUR)
        self.s3_fh.seek(to_previous_chunk, io.SEEK_CUR)
        self.assertEqual(self.local_fh.read(len_10pct), self.s3_fh.read(len_10pct), 'overlapping chunk mismatch!')

        # Compare bytes 1/4 way through file
        pos_25pct = int(math.floor(self.file_size * 0.25))
        self.local_fh.seek(pos_25pct)
        self.s3_fh.seek(pos_25pct)
        self.assertEqual(self.local_fh.read(len_10pct), self.s3_fh.read(len_10pct), '25-35pct byte range mismatch!')

        # Compare entire file
        self.local_fh.seek(0)
        self.s3_fh.seek(0)
        self.assertEqual(self.local_fh.read(), self.s3_fh.read(), 'bytes mismatch!')

if __name__ == '__main__':
    unittest.main(verbosity=2)
